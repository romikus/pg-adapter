import { Creds, Socket } from '../types';
export declare const handleMessage: (socket: Socket, creds: Creds) => void;
export declare const removeListener: (socket: Socket) => void;
