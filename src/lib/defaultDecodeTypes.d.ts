/// <reference types="node" />
export declare const defaultDecodeTypes: {
    20: (value: Buffer, pos: number, size: number) => number;
    21: (value: Buffer, pos: number, size: number) => number;
    23: (value: Buffer, pos: number, size: number) => number;
    700: (value: Buffer, pos: number, size: number) => number;
    701: (value: Buffer, pos: number, size: number) => number;
    1700: (value: Buffer, pos: number, size: number) => number;
    1560: (value: Buffer, pos: number, size: number) => number;
    1562: (value: Buffer, pos: number, size: number) => number;
    16: (value: Buffer) => boolean;
    1082: (value: Buffer, pos: number, size: number) => Date;
    1114: (value: Buffer, pos: number, size: number) => Date;
    1184: (value: Buffer, pos: number, size: number) => Date;
};
