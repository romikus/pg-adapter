export declare const sql: (parts: string | TemplateStringsArray, ...args: any[]) => string;
export declare const sql2: (parts: string | TemplateStringsArray, args?: any[] | undefined) => string;
