"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResultMode = void 0;
var ResultMode;
(function (ResultMode) {
    ResultMode[ResultMode["objects"] = 0] = "objects";
    ResultMode[ResultMode["arrays"] = 1] = "arrays";
    ResultMode[ResultMode["value"] = 2] = "value";
    ResultMode[ResultMode["skip"] = 3] = "skip";
})(ResultMode = exports.ResultMode || (exports.ResultMode = {}));
